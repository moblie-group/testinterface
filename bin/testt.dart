import 'package:testt/testt.dart' as testt;

void main(List<String> arguments) {
  // Subclass s1 = Subclass();
  // s1.display();
  var all1 = Alligator();
  all1.hunt();
}


class Interface {
  void display() {
    print(" This is the function of the interface class. ");
  }
}
class Subclass implements Interface {
  @override
  void display() {
    print(" This is the function of the subclass. ");
  }
}
class Alligator extends Reiptile {
  
}
class Crocodile extends Reiptile {
 
}
abstract class Reiptile{

  void swim() => print('Swimming');
  void bite() => print('Chomp');
  void crawl() => print('Crawling');
  void hunt() {
    print('Alligator -------');
    swim();
    crawl();
    bite();
    print('Eat Fish');
  }
}
